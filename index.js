const botconfig = require("./botconfig.json");
const Discord = require("discord.js");
const os = require("os");
const bot = new Discord.Client({disableEveryone: true});

bot.on("ready", async () =>{
	console.log(`${bot.user.username} is running!`)
	bot.user.setActivity(os.hostname()+" "+os.uptime(), );
});

var forbidenWords = ["!play","Assassination","Assault","Battery","Child abuse","Criminal negligence","Defamation","False imprisonment","Harassment","Home invasion","Homicide","Intimidation","Kidnapping","Manslaughter","Mayhem","Murder","Negligent homicide","Invasion of privacy","Robbery","Torture","Pedo","loli"];
var points = 0;

bot.on("message", async message => {
	if(message.author.bot) return;
	if(message.channel.type === "dm") return;

	let prefix = botconfig.prefix;
	let messageArray = message.content.split(" ");
	let cmd = messageArray[0];
	let args = messageArray.slice(1);

	for (var i = 0; i < forbidenWords.length; i++) {
  		if (message.content.includes(forbidenWords[i])) {
  			message.reply("");
  			points++;
  			message.reply(points);
  			break;
  		}
  		if (message.content.includes(forbidenWords[i].toUpperCase())) {
  			message.reply("");
  			points++;
  			message.reply(points);
  			break;
  		}
  		if (message.content.includes(forbidenWords[i].toLowerCase())) {
  			message.reply("");
  			points++;
  			message.reply(points);
  			break;
  		}
	}

	if (cmd === prefix+"points"){
		message.reply("points this session: "+points);
	}
  if (cmd === prefix+"tiptap"){
    message.reply("tiptap");
  }
});
bot.login(botconfig.token);